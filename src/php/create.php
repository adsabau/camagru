<?php
session_start();
$canvas = $_POST['img'];
$gif = $_POST['gif'];

if (!isset ($_SESSION['i'])){
    $_SESSION['i'] =0;
}

save_base64_image($canvas);

function save_base64_image($base64_image_string, $path_with_end_slash="" ) {
    $data = explode( ',', $base64_image_string );
    $data = str_replace(' ', '+', $data[1]);    
    $img = base64_decode($data);
    file_put_contents("../output/img.png", $img);
}

$img1 = imagecreatefrompng('../output/img.png');
$img2 = imagecreatefromgif($gif);


if(imagealphablending($img2, true) && imagealphablending($img1, true)){

    imagecopy($img1, $img2, 200, 150, 0, 0, 200, 200); //have to play with these numbers for it to work for you, etc.
    imagepng($img1,'../output/png'.++$_SESSION['i'].'.png');
    imagedestroy($img1);
    imagedestroy($img2);
    echo('png'.$_SESSION['i'].'.png');
    
} else {
    echo("error");
}


