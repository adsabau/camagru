<?php
	session_start();
	$layout = '';
	$url = (isset($_GET['url'])) ? $_GET['url'] : ((isset($_POST['url'])) ? $_POST['url'] : '');
	$username = $_SESSION['login_user'];
	
	switch ($url)
	{
		// Profile
		case 'profile':
			$layout = 'src/templates/template_profile.php';
			require('src/php/profile.php');
			break;

		// Picture
		case 'picture':
			$layout = 'src/templates/template_picture.php';
			require('src/php/picture.php');
			break;

		// Chat
		case 'chat':
			$layout = 'src/templates/template_chat.php';
			require('src/php/chat.php');
			break;

		// Login
		case '':
			$layout = 'src/templates/template_login.php';
			require('src/php/login.php');
            break;
        
        // Photoshoot
		case 'photoshoot':
        $layout = 'src/templates/template_photoshoot.php';
        require('src/php/photoshoot.php');
        break;

        // Registration
		case 'registration':
        $layout = 'src/templates/template_registration.php';
        require('src/php/registration.php');
		break;
		
		// Verification
		case 'verification':
        $layout = 'src/templates/template_verification.php';
        require('src/php/verification.php');
        break;

		// Logout
		case 'logout':
			require('src/php/logout.php');
			break;

		// 404 - Page not Found
		default:
			$layout = 'src/templates/template_404.php';
			break;
	}
	?>

<script>
	var username = "<?php echo $_SESSION['login_user'] ?>";
</script>