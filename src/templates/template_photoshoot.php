<div class='photoshoot-container'>
    <div class='camera' id='camera'></div>
    <div class='container'>
        <div class='center'>
            <div>
                <div class='overlay' id='overlay'></div>
                <video autoplay="true" class="videoElement" id='videoElement'></video>  
            </div>
        </div>
    </div>
    <div class='menu-photoshoot' id='menu-phtoshoot'>
        <div class='list-gif' id='listGif'>
            <img id='gif1' class='gif' src="src/gifs/beard.gif" onclick='reply_click(this.id)'>
            <img id='gif2' class='gif' src="src/gifs/blue.gif" onclick='reply_click(this.id)'>
            <img id='gif3' class='gif' src="src/gifs/car.gif" onclick='reply_click(this.id)'>
            <img id='gif4' class='gif' src="src/gifs/cat.gif" onclick='reply_click(this.id)'>
            <img id='gif5' class='gif' src="src/gifs/dog.gif" onclick='reply_click(this.id)'>
            <img id='gif6' class='gif' src="src/gifs/glasses.gif" onclick='reply_click(this.id)'>
            <img id='gif7' class='gif' src="src/gifs/haircut.gif" onclick='reply_click(this.id)'>
            <img id='gif8' class='gif' src="src/gifs/haircut2.gif" onclick='reply_click(this.id)'>
            <img id='gif9' class='gif' src="src/gifs/happyCloud.gif" onclick='reply_click(this.id)'>
            <img id='gif10' class='gif' src="src/gifs/hello.gif" onclick='reply_click(this.id)'>
            <img id='gif11' class='gif' src="src/gifs/moustache.gif" onclick='reply_click(this.id)'>
            <img id='gif13' class='gif' src="src/gifs/winkCloud.gif" onclick='reply_click(this.id)'>
            <img id='gif15' class='gif' src="src/gifs/glasses2.gif" onclick='reply_click(this.id)'>
            <img id='gif16' class='gif' src="src/gifs/hat.gif" onclick='reply_click(this.id)'>
            <img id='gif17' class='gif' src="src/gifs/moustache2.gif" onclick='reply_click(this.id)'>
            <img id='gif18' class='gif' src="src/gifs/vampire.gif" onclick='reply_click(this.id)'>
        </div>
        <div class='shoot-button' id='shotButton' onclick="takeSnap();">
            <img src='src/incons/takePhoto.png'>
        </div>
    </div>
    
    
    <div class = 'sp'>
        <img id='snap' class = 'snap' src=''>
        <canvas class='c' id="c" width="800" height="600"></canvas></bd>
    </div>
    <div id='uploadPic' class='front_button upload' onclick='uploadPHP();'>Upload</dev>
    
</div> 

<script>

    var snap = document.getElementById('snap');
    var video = document.getElementById("videoElement");
    var canvas = document.getElementById("c");
    var pic = document.getElementById('uploadPic');
    var overlay = document.getElementById("overlay");

    var activeGif = '';

    snap.style.display = 'none';

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
  
    if (navigator.getUserMedia) {       
        navigator.getUserMedia({video: true}, handleVideo, (function(){}));
    }

    function handleVideo(stream) {
        video.src = window.URL.createObjectURL(stream);
    }
  
    function takeSnap(){

        snap.style.display = 'none';

        if (document.getElementById('createdGif')) {

            canvas.getContext('2d').drawImage(video, 0, 0);
            pic.style.display = 'flex';

            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", 'src/php/create.php', true);
            xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhttp.send("img="+canvas.toDataURL()+"&gif="+activeGif);
            
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    console.warn(this.responseText);
                    document.getElementById('snap').src = 'src/output/'+this.responseText;
                    document.getElementById('snap').style.display = 'block';
                }
            };
        } else {
            alert('Please select one of the GIFs')
        }
    }
    
    function reply_click(clicked_id)
    {
        var child = document.getElementById('createdGif');
        if (child) {
            overlay.removeChild(child);
        }
        var id = clicked_id;
        var src = activeGif = document.getElementById(id).src;
        var gif = document.createElement("IMG");
        gif.id = 'createdGif';
        gif.setAttribute("src", src);
        gif.setAttribute("height", "200");
        gif.setAttribute("width", "auto");
        overlay.appendChild(gif);
        overlay.style.display = 'block';
    }

    function uploadPHP(){
        console.log('intra');
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", 'src/php/upload.php', true);
        xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhttp.send();
        console.log('iese');
                
    }
</script>