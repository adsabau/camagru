<div class='singup-container'>
    <div class='singup' id='singuUp'><span>Sing<span>Up</span></span></div>
    <div class='form-container'>
        <form class='register-form' id='registrationForm' name='registrationForm' action='' method='post'>
            <div class='input-container'>
                <label class='register-label' for='username'>Username:</label>
                <input class='register-input' id='usernameR' type='text' name='username' value='' placeholder='Please chose a username'>
            </div>    
            <div class='input-container'>
                <label class='register-label' for='email'>Email:</label>
                <input class='register-input' id='emailR' type='email' name='email' value='' placeholder='someone@somewhere.something'>
            </div>    
            <div class='input-container'>
                <label class='register-label' for='password'>Password:</label>
                <input class='register-input' id='passwordR' type='password' name='password' value='' placeholder='Choose a password'>
            </div>    
                <input type="submit" class="front_button" value="Sign up" onclick='submitForm();'/>
            <a class='link' id='backToLogin'>You already have an account? CLICK here</a>
        </form>

    <div id="myModal" class="modal">
  <!-- Modal content -->
  <div class="error-content">
    <span class="close">&times;</span>
</div>

</div>
    </div>
</div>
