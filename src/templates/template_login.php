<div class='login-container'>
    <div class='camagru-login'><span>Camagru</span></div>
    <div class='login-form-container'>
        <form class='login-form' action='' method='post'>
            <div class='login-input-container'>
                <label class='login-label' for='username'>Username:</label>
                <input id='loginInput' class='login-input' type='text' name='username' value='' placeholder='Enter your username'>
            </div>    
            <div class='login-input-container'>
                <label class='login-label' for='password'>Password:</label>
                <input class='login-input' type='password' name='password' value='' placeholder='Choose a password'>
            </div>    
            <div  class='login-button-container'>
                <input class='front_button' type="submit" class="submit_button" value="Sign in" />
                <div id='create_button' class='front_button'><span id='registerButton'>Create Account</span></div>                
            </div>
        </form>
    </div>
</div>
