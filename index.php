<?php
    require('src/includes/db.php');
    require('src/php/routes.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Camagru</title>
    <link rel='icon' href='src/incons/camagru.png'/>
    <?php require('src/includes/linksCSS.php');?>
</head>
<body class='index-body'>
    <div class='page' id='page'>
        <div class='header' id='header' value='1'>
            <?php require('src/includes/header.php');?>
        </div>
        <div class='content'>
            <?php require($layout); ?>
        </div>
        <div class='footer' id='footer'>
            <?php require('src/includes/footer.php');?>
        </div>
    </div>
    <?php require('src/includes/linksJS.php');?>
</body>
</html>