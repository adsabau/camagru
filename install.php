
<?php 

  

if(isset($_POST['install'])){ // button name
    
    install();
}

function install(){
    
    //change variable for any other DB
        $host = 'localhost';
        $user = 'root';
        $password = '';
        $db = 'camagru';


    $connection = mysqli_connect($host, $user, $password);

    //create DB is not exist 
    $sqlDB = "CREATE DATABASE IF NOT EXISTS `".$db."`";

    $sqlTable = "CREATE TABLE `users` (
        `id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
        `username` VARCHAR( 32 ) NOT NULL ,
        `password` VARCHAR( 32 ) NOT NULL ,
        `email` TEXT NOT NULL ,
        `hash` VARCHAR( 32 ) NOT NULL ,
        `active` INT( 1 ) NOT NULL DEFAULT '0'
        ) ENGINE = MYISAM ;";

    if(mysqli_query($connection, $sqlDB)){
        mysqli_select_db($connection, $db);
        mysqli_query($connection,$sqlTable);
    }
}?>
<head>    
    <?php require('src/includes/linksCSS.php');?>
    <style>
        .container-install{
            height: 100%;
            width: 100%;
            display: grid;
            grid-template-row: .8fr .2fr;
        }
        .container-button-install{
            margin: auto;
        }
        .install-notes{
            margin-left: 25px;
        }
        .instructions{
            font-family: joe;
            font-size: 30px;
        }

        .from-install-container{
            width: 100%;
            height: 100%;
            display:flex;
        }
        .install-form{
            margin:auto;
        }
        .install-container{
            display:none;
            margin: auto;
        }
        .btninstall{
            margin-top: 200px;
            text-align: center;
        }
    </style>
</head>
<body class= 'container-install' >
    <div class="from-install-container" id='fromInstallContainer'>
        <form id='installForommm' class='install-form' action='' method='post'>
            <input type = "button" class="front_button" value="intall" name='install' onclick='changeFunction()'>
        </form>
    </div> 
    <div id='installContainer' class='install-container'>
        <div class = 'install-notes'>
            <a class='instructions'> After instalation is complete copy the .ini files from camagru\resources\ to the following paths:</a>
            </br>
            </br><a class='instructions'>    1) php.ini  into xampp\php\ </a>
            </br><a class='instructions'>    2) sendmail.ini into xampp\sendmail\</a> 
        </div>
        <div class='container-button-install'>
            <div class="front_button btninstall" onclick='installComplete();'><span>install successfully</span></div>
        </div>
    </div>
</body>

<script>
    function changeFunction(){
        document.getElementById('installForommm').submit;
        document.getElementById('fromInstallContainer').style.display = "none"
        document.getElementById('installContainer').style.display = "block"
        
    }

    function installComplete(){
        window.location.pathname = "/camagru/index.php";
    }
</script>


